class Validator:
    """
    Validates Mastermind guesses and offers hints of (partially correct) tries.
    """

    CODE_LENGTH: int = 4
    CODE_LETTERS = ['A', 'B', 'C', 'D', 'E', 'F']

    YELLOW = "YELLOW"
    GREEN = "GREEN"

    CORRECT_GUESS = GREEN + GREEN + GREEN + GREEN
    NO_MATCHES = ''

    def __init__(self, code: str):
        """
        Initialize this validator with the code that has to be guessed during the game of Mastermind.
        :param code correct code that guesses will be validated againstAAA
        """
        self.__code = code
        if not Validator.check_input(code):
            raise RuntimeError('Invalid code')

    @staticmethod
    def check_input(input_value: str) -> bool:
        if len(input_value) != Validator.CODE_LENGTH:
            return False
        for ch in input_value:
            if ch not in Validator.CODE_LETTERS:
                return False
        return True

    def validate(self, guess: str):
        """
      Validate the guess against the code and check if it contains correct characters. Every correct character adds
      Validator.YELLOW to the resulting String. If the correct character sits on the right index in the guess,
      Validator.GREEN will be added to the result.

      Example: Code "ABCD" and guess "EACE" return Validator.YELLOW (A on the wrong index)
      and Validator.GREEN (C on the right index).

      :param guess guess that should be checked against the correct code.
      :return String with the Validator.YELLOW and Validator.GREEN hints set; return empty String for incorrect guess
     """
        return None
