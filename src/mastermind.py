import random
import subprocess

from validator import Validator

subprocess.call('', shell=True)


class Mastermind:
    class Colors:
        GREEN = '\033[92m'  # GREEN
        YELLOW = '\033[93m'  # YELLOW
        RESET = '\033[0m'  # RESET COLOR

    MAX_NUMBER_OFF_TRIES = 8

    def play(self):
        self.__intro()
        code = self.__create_code()
        validator = Validator(code)
        for i in range(Mastermind.MAX_NUMBER_OFF_TRIES):
            print(f"Attempt {i + 1}:")
            guess = Mastermind.get_input()
            feedback: str = validator.validate(guess)
            self.__print_feedback(feedback)
            if feedback == Validator.CORRECT_GUESS:
                self.__win()
                exit()
        self.__lose()
        print(f'correct code:{code}')

    @staticmethod
    def __print_feedback(feedback: str):
        if feedback is None:
            print("Validate method in the Validator class is not implemented!")
            return
        feedback = Mastermind.__add_color_escape_seq(feedback)
        print(feedback)

    @staticmethod
    def __add_color_escape_seq(feedback):
        feedback = feedback.replace(Validator.YELLOW,
                                    (Mastermind.Colors.YELLOW + Validator.YELLOW + Mastermind.Colors.RESET))
        feedback = feedback.replace(Validator.GREEN,
                                    (Mastermind.Colors.GREEN + Validator.GREEN + Mastermind.Colors.RESET))
        return feedback

    @staticmethod
    def get_input():
        guess = input().strip()
        while not Validator.check_input(guess):
            print("Please enter a four-letter code from letters A-F (e.g. 'EDEA')")
            guess = input().strip()
        return guess

    @staticmethod
    def __create_code() -> str:
        selected_entries = random.choices(Validator.CODE_LETTERS, k=Validator.CODE_LENGTH)
        return ''.join(selected_entries)

    @staticmethod
    def __intro():
        yellow = Mastermind.__add_color_escape_seq(Validator.YELLOW)
        green = Mastermind.__add_color_escape_seq(Validator.GREEN)
        print("Welcome to")
        print("    __  ___           __                      _           __")
        print("   /  |/  /___ ______/ /____  _________ ___  (_)___  ____/ /")
        print("  / /|_/ / __ `/ ___/ __/ _ \\/ ___/ __ `__ \\/ / __ \\/ __  /")
        print(" / /  / / /_/ (__  ) /_/  __/ /  / / / / / / / / / / /_/ /")
        print("/_/  /_/\\__,_/____/\\__/\\___/_/  /_/ /_/ /_/_/_/ /_/\\__,_/")
        print("The computer has created a four-letter code from letters A-F (e.g. 'EDEA')")
        print("You have eight guesses to crack it.")
        print(
            f"After each guess you will receive feedback in the form {yellow} or {green}")
        print(f"right character, right position: {green}")
        print(f"right character, wrong position: {yellow}")
        print(
            f"Example code: ADAE\nYour guess: BEAF\nFeedback: {green}{yellow} (A: right character,"
            f" right position, E: right character, wrong position)\n")

    @staticmethod
    def __win():
        print("Congratulations! You cracked the code!")

    @staticmethod
    def __lose():
        print("You lost! What a pity. Maybe give it another try...")


if __name__ == "__main__":
    Mastermind().play()
