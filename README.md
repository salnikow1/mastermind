# Mastermind TDD Kata

This simple console application implements the game of Mastermind.

## Preconditions

at least python 3.7

## Play

python.exe ...\mastermind.py

## Test

python.exe ..\test_validator.py
